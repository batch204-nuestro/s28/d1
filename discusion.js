db.users.insertOne({
	"firstName": "jane",
	"lastName": "Doe",
	"age":21,
	"email": "janedoe@gmail.com",
	"department": "none"
})

db.users.inserMany([
		{
			"firstName": "jane",
			"lastName": "Doe",
			"age":21,
			"email": "janedoe@gmail.com",
			"department": "none"
		},
		{
			"firstName": "Ann",
			"lastName": "Maya",
			"age":21,
			"email": "amaya@gmail.com",
			"department": "none"
		},
		{
			"firstName": "rick",
			"lastName": "Shue",
			"age":21,
			"email": "rshue@gmail.com",
			"department": "none"
		}
	])

db.courses.insertMany ([
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "Javascript 101",
			"price": 5000,
			"description": "Introduction to Javascript",
			"isActive": true
		},
		{
			"name": "CSS 101",
			"price": 2500,
			"description": "Introduction to CSS",
			"isActive": false
		},
	]);

// Find Document
db.users.find();


"email": "amaya@gmail.com"

// Update one

db.courses.updateOne(
		{
			"name": "Javascript 101"
		},
		{
			$set:{
				"isActive":false
			}
		}

	)

// update many

db.courses.updateMany(
    {},
            {
            $set:{
            "enrollees":10}
            }
)

// deleting docs / ONE

db.users.insertOne({
	"firstName": "Test"
});

db.users.deleteOne({
	"firstName": "Test"
})

db.users.updateMany(
		{
			"age": 21
		},
		{
			$set:{
				"department":"HR"
			}
		}

	)

db.users.deleteMany({"dept": "HR"})
db.users.deleteMany({});